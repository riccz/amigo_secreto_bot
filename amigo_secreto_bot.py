#!/usr/bin/env python3

import configparser
import logging
import math
import operator
import re


from telegram.ext import (
    CommandHandler,
    Updater,
)

from backend import AmigoSecreto

config = configparser.ConfigParser()
config.read("amigo_secreto_bot.config")

ERR_MARK = "❌"
OK_MARK = "✅"

AS = None
LOGGER = logging.getLogger("main")
BOT_USER = None


class TgError(RuntimeError):
    pass


def format_list_lines(lines):
    lines = list(lines)
    n = len(lines)
    if n < 1:
        return ""

    n_digits = math.floor(math.log10(n)) + 1
    return "\n".join(f"{i:{n_digits}d}. {line}" for i, line in enumerate(lines, 1))


def format_as_list(as_list):
    users = as_list.users()
    themes = as_list.themes()

    reply_text = "*Partecipanti*\n\n"

    reply_text += format_list_lines(
        f"[{u.name if u.name else u.user_id}](tg://user?id={u.user_id:d})"
        for u in sorted(users, key=operator.attrgetter("name"))
    )

    reply_text += "\n\n*Temi*\n\n"

    reply_text += format_list_lines(themes)
    return reply_text


def with_as_context(f):
    def wrapper(bot, update, *args, **kwargs):
        # The handlers run in different thread and sqlite doesn't like that. Workaround by creating the AS instance in the handler thread
        # TODO: actual fix: use a threadsafe connection pool. Or use the persistence tools from the telegram lib
        global AS
        if AS is None:
            AS = AmigoSecreto(config["default"]["sqlite_db"])

        if update.message:
            kwargs["as_list"] = AS.get_list(chat_id=update.message.chat_id)
            kwargs["as_user"] = AS.get_user(user_id=update.message.from_user.id)

            if kwargs["as_list"] and kwargs["as_user"]:
                kwargs["is_admin"] = kwargs["as_list"].is_admin(kwargs["as_user"])
            else:
                kwargs["is_admin"] = None

        return f(bot, update, *args, **kwargs)

    return wrapper


def require_admin(f):
    def wrapper(bot, update, *args, is_admin=False, **kwargs):
        if not is_admin:
            raise TgError("Non sei un admin")
        return f(bot, update, *args, **kwargs)

    return wrapper


def get_join_link(bot, as_list):
    global BOT_USER
    if BOT_USER is None:
        BOT_USER = bot.get_me()

    token = as_list.join_token
    assert re.fullmatch(r"[0-9a-z-]+", token, re.I)

    botname = BOT_USER.username
    assert botname

    return f"https://telegram.me/{botname}?start={token}"


@with_as_context
def start(bot, update, args, as_list=None, **_):
    if args:
        join_token = args[0]
        as_list = AS.get_list(join_token=join_token)
        if not as_list:
            join_text = " ".join([ERR_MARK, "Nessuna lista corrisponde al token"])
        else:
            user = AS.create_user(
                user_id=update.message.from_user.id,
                private_chat_id=update.message.chat_id,
                name=update.message.from_user.full_name,
            )
            as_list.add_user(user)
            as_list.associate_chat(user.private_chat_id)
            AS.commit()

            list_name = (
                f'alla lista "{as_list.name}"'
                if as_list.name
                else "a una lista senza nome"
            )
            join_text = " ".join([OK_MARK, "Sei stato aggiunto ", list_name])
    else:
        join_text = """Comandi:
        /create_list [name] - crea una nuova lista
        /draw [msg] - esegui l'estrazione e invia il risultato ai partecipanti, con un messaggio opzionale
        /reset - cancella la lista dei partecipanti
        /set_list_name - dai un nome alla lista
        /show - mostra la lista dei partecipanti
        /start - mostra questo messaggio
        /add_theme <theme> - aggiungi un tema
        /remove_theme <theme> - rimuovi un tema
        """

    bot.send_message(chat_id=update.message.chat_id, text=join_text)


@with_as_context
def create_list(bot, update, args, as_list=None, **_):
    name = None
    if args:
        name = " ".join(args)

    as_list = AS.create_list(name=name)
    as_list.associate_chat(update.message.chat_id)

    user = AS.create_user(
        user_id=update.message.from_user.id,
        private_chat_id=update.message.chat_id,
        name=update.message.from_user.full_name,
    )
    as_list.add_user(user, is_admin=True)
    as_list.associate_chat(user.private_chat_id)

    AS.commit()

    join_link = get_join_link(bot, as_list)
    name = (
        f'la lista "{as_list.name}"'
        if as_list.name is not None
        else "una lista senza nome"
    )
    reply_text = " ".join(
        [
            OK_MARK,
            f"Hai creato {name}\n\nInvita i membri con questo link: [{join_link}]({join_link})",
        ]
    )

    bot.send_message(
        chat_id=update.message.chat_id,
        text=reply_text,
        parse_mode="Markdown",
        reply_to_message_id=update.message.message_id,
    )


@with_as_context
@require_admin
def set_list_name(bot, update, args, **_):
    name = None
    if args:
        name = " ".join(args)
    the_list = AS.get_list(chat_id=update.message.chat_id)
    the_list.name = name
    the_list.update()
    AS.commit()
    reply_text = "Ora la lista si chiama '{}'".format(name)
    bot.send_message(
        chat_id=update.message.chat_id,
        text=reply_text,
        reply_to_message_id=update.message.message_id,
    )


@with_as_context
def subscribe(bot, update, **_):
    the_list = AS.get_list(chat_id=update.message.chat_id)
    the_user = AS.get_user(user_id=update.message.from_user.id)
    if not the_user:
        reply_text = """Per unirti alla lista puoi usare [questo link](https://telegram.me/amigosecrebot?start={!s}
        """.format(
            the_list.join_token
        )
    else:
        the_list.add_user(the_user)
        AS.commit()
        reply_text = "OK"
    bot.send_message(
        chat_id=update.message.chat_id,
        text=reply_text,
        parse_mode="Markdown",
        reply_to_message_id=update.message.message_id,
    )


@with_as_context
def unsubscribe(bot, update, **_):
    the_list = AS.get_list(chat_id=update.message.chat_id)
    the_user = AS.get_user(user_id=update.message.from_user.id)
    if the_user:
        the_list.remove_user(the_user)
        AS.commit()

    reply_text = "OK"
    bot.send_message(
        chat_id=update.message.chat_id,
        text=reply_text,
        reply_to_message_id=update.message.message_id,
    )


@with_as_context
@require_admin
def reset(bot, update, **_):
    the_list = AS.get_list(chat_id=update.message.chat_id)
    if the_list:
        the_list.delete()
        AS.commit()
    reply_text = "OK"
    bot.send_message(
        chat_id=update.message.chat_id,
        text=reply_text,
        reply_to_message_id=update.message.message_id,
    )


@with_as_context
def show(bot, update, as_list, **_):
    if not as_list:
        raise TgError("Crea o unisciti a una lista")

    bot.send_message(
        chat_id=update.message.chat_id,
        text=format_as_list(as_list),
        parse_mode="Markdown",
        reply_to_message_id=update.message.message_id,
    )


@with_as_context
@require_admin
def draw(bot, update, args, as_list=None, as_user=None, **_):
    if args:
        msg = f"[{as_user.name or as_user.user_id}](tg://user?id={as_user.user_id}): {' '.join(args)}"
    else:
        msg = None

    if not as_list:
        raise TgError("Crea o unisciti a una lista")
    if len(as_list.users()) < 2:
        raise TgError("Meno di 2 partecipanti")

    header = f"*Estrazione di \"{as_list.name if as_list.name else 'una lista senza nome'}\"*"

    pairs = AS.draw(list_id=as_list.list_id)

    for p in pairs:
        to_name = str(p["to"].name or p["to"].user_id)
        to_link = f"tg://user?id={p['to'].user_id}"
        theme_str = f"\"{p['theme']}\"" if p["theme"] else "libero"
        private_txt = f"🎅 Sei il secret santa di [{to_name}]({to_link})!\n🎁 Il tema del regalo è {theme_str}!"

        msg_parts = [header, private_txt]
        if msg:
            msg_parts.append(msg)

        bot.send_message(
            chat_id=p["from"].private_chat_id,
            text="\n\n".join(msg_parts),
            parse_mode="Markdown",
        )

    bot.send_message(
        chat_id=update.message.chat_id,
        text=f"{OK_MARK} Inviato i messaggi ai partecipanti",
        parse_mode="Markdown",
        reply_to_message_id=update.message.message_id,
    )


@with_as_context
def add_theme(bot, update, args, as_list=None, **_):
    joined_args = " ".join(args)
    themes = []
    for theme in joined_args.split(";"):
        theme = theme.strip()
        if not theme:
            continue
        as_list.add_theme(theme)
        themes.append(theme)
    AS.commit()

    bot.send_message(
        chat_id=update.message.chat_id,
        text=f"{OK_MARK} Aggiunto i temi:\n{format_list_lines(themes)}",
        parse_mode="Markdown",
        reply_to_message_id=update.message.message_id,
    )


@with_as_context
def remove_theme(bot, update, args, as_list=None, **_):
    theme = " ".join(args)
    as_list.remove_theme(theme)
    AS.commit()

    bot.send_message(
        chat_id=update.message.chat_id,
        text=" ".join([OK_MARK, f"Rimosso il tema {theme}"]),
        parse_mode="Markdown",
        reply_to_message_id=update.message.message_id,
    )


logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)


def error_handler(bot, update, error):
    LOGGER.error("Default error handler: %r", error, exc_info=error)

    if isinstance(error, TgError):
        error_message = str(error)
    else:
        error_message = "Errore generico"

    if update.message:
        bot.send_message(
            chat_id=update.message.chat_id,
            text=" ".join([ERR_MARK, error_message]),
            parse_mode="Markdown",
            reply_to_message_id=update.message.message_id,
        )


updater = Updater(token=config["default"]["telegram_token"])
dispatcher = updater.dispatcher

dispatcher.add_handler(CommandHandler("create_list", create_list, pass_args=True))
dispatcher.add_handler(CommandHandler("draw", draw, pass_args=True))
dispatcher.add_handler(CommandHandler("reset", reset))
dispatcher.add_handler(CommandHandler("set_list_name", set_list_name, pass_args=True))
dispatcher.add_handler(CommandHandler("show", show))
dispatcher.add_handler(CommandHandler("start", start, pass_args=True))
# dispatcher.add_handler(CommandHandler("subscribe", subscribe))
# dispatcher.add_handler(CommandHandler("unsubscribe", unsubscribe))
dispatcher.add_handler(CommandHandler("add_theme", add_theme, pass_args=True))
dispatcher.add_handler(CommandHandler("remove_theme", remove_theme, pass_args=True))


dispatcher.add_error_handler(error_handler)

updater.start_polling(poll_interval=1)
