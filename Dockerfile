FROM python:3.8

ENV UID=nobody
ENV GID=nogroup

RUN apt-get update && apt-get install -y \
    gosu \
    && rm -rf /var/lib/apt/lists/*

COPY docker-entrypoint.sh /docker-entrypoint.sh

WORKDIR /app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

VOLUME /data

ENTRYPOINT [ "/docker-entrypoint.sh" ]
CMD [ "python", "./amigo_secreto_bot.py" ]
