import pytest
from hypothesis import given, strategies as st

from backend import argsort, santa_shuffle


class TestArgSort:
    @given(st.lists(st.integers()))
    def test_argsort_indices(self, xs):
        idxs = argsort(xs)
        assert [xs[i] for i in idxs] == sorted(xs)


class TestSantaShuffle:
    @pytest.mark.parametrize("i", [-100, -1, 0, 1])
    def test_count_too_small(self, i):
        with pytest.raises(AssertionError):
            santa_shuffle(i)

    @given(st.random_module())
    def test_min_count(self, _):
        conns = santa_shuffle(2)
        assert conns == [1, 0]

    @given(st.random_module())
    def test_with_3(self, _):
        targets = santa_shuffle(3)
        cases = [[1, 2, 0], [1, 0, 2], [2, 0, 1]]
        assert any(targets == c for c in cases)

    @given(st.random_module(), st.integers(min_value=4, max_value=1000))
    def test_is_derangement(self, _, n):
        targets = santa_shuffle(n)
        assert sorted(targets) == list(range(n))
        assert all(i != j for i, j in enumerate(targets))
