#!/bin/bash

set -e

if [ "$1" = 'python' ]; then
    chown -R "$UID:$GID" /data
    exec gosu "$UID:$GID" "$@"
fi

exec "$@"
