import logging
import random
import sqlite3
import uuid


LOGGER = logging.getLogger(__name__)


CREATE_TABLES_SCRIPT = """
begin transaction;

create table if not exists lists (
  list_id integer primary key autoincrement,
  join_token text unique not null,
  name text
);

create table if not exists themes (
  list_id integer not null references lists (list_id) on delete cascade,
  name text not null,
  primary key (list_id, name)
);

create table if not exists users (
  user_id integer primary key,
  private_chat_id integer unique not null,
  name text
);

create table if not exists memberships (
  list_id integer not null,
  user_id integer not null,
  is_admin boolean not null default false,
  primary key (list_id, user_id),
  foreign key (list_id) references lists (list_id) on delete cascade,
  foreign key (user_id) references users (user_id) on delete cascade
);

create table if not exists chats (
  chat_id integer primary key,
  list_id not null,
  foreign key (list_id) references lists (list_id) on delete cascade
);

commit;

pragma foreign_keys = on;
"""


def argsort(xs):
    sorted_ixs = sorted(enumerate(xs), key=lambda ix: ix[1])
    return [i for (i, _) in sorted_ixs]


def santa_shuffle(count):
    assert count >= 2, "Cannot santa-shuffle less than 2 persons"

    targets = list(range(count))
    random.shuffle(targets)
    sources = [targets[-1]] + targets[:-1]

    sort_idxs = argsort(sources)
    return [targets[i] for i in sort_idxs]


class ASList:
    @staticmethod
    def create(cursor, name=None):
        new_join_token = str(uuid.uuid4())
        cursor.execute(
            "insert into lists (join_token, name) values (?, ?) returning list_id",
            (new_join_token, name),
        )
        ((new_list_id,),) = cursor.fetchall()
        LOGGER.debug("Created new list: %s (name = %s)", new_list_id, name)
        return ASList(cursor, new_list_id, new_join_token, name)

    @staticmethod
    def get(cursor, list_id=None, join_token=None, chat_id=None):
        assert sum(1 for arg in [list_id, join_token, chat_id] if arg is not None) == 1
        if list_id is not None:
            cursor.execute(
                "select list_id, join_token, name from lists where list_id = ?",
                (list_id,),
            )
        elif join_token is not None:
            cursor.execute(
                "select list_id, join_token, name from lists where join_token = ?",
                (join_token,),
            )
        elif chat_id is not None:
            cursor.execute(
                "select list_id, join_token, name"
                " from lists where list_id in"
                " (select list_id from chats"
                " where chat_id = ?)",
                (chat_id,),
            )
        l = cursor.fetchone()
        if not l:
            return None
        else:
            return ASList(cursor, *l)

    def __init__(self, cursor, list_id, join_token, name):
        self.__cursor = cursor
        self.__list_id = list_id
        self.__join_token = join_token
        self.__admins = None
        self.name = name

    @property
    def list_id(self):
        return self.__list_id

    @property
    def join_token(self):
        return self.__join_token

    def delete(self):
        self.__cursor.execute("delete from lists where list_id = ?", (self.list_id,))

    def update(self):
        self.__cursor.execute(
            "update lists set name = ? where list_id = ?", (self.name, self.list_id)
        )

    def users(self):
        self.__cursor.execute(
            "select users.user_id, private_chat_id, name"
            " from users join memberships"
            " on users.user_id = memberships.user_id"
            " where list_id = ?",
            (self.list_id,),
        )
        us = self.__cursor.fetchall()
        return [ASUser(self.__cursor, *u) for u in us]

    def add_user(self, user, is_admin=False):
        self.__cursor.execute(
            "insert or ignore into memberships (list_id, user_id, is_admin)"
            " values (?, ?, ?)",
            (self.list_id, user.user_id, is_admin),
        )

    def remove_user(self, user):
        self.__cursor.execute(
            "delete from memberships where list_id = ? and user_id = ?",
            (self.list_id, user.user_id),
        )

    def associate_chat(self, chat_id):
        self.__cursor.execute(
            "insert or replace into chats (chat_id, list_id) values (?, ?)",
            (chat_id, self.list_id),
        )

    def add_theme(self, theme):
        assert len(theme) > 0, "Empty theme"
        try:
            self.__cursor.execute(
                "insert into themes (list_id, name) values (?, ?)",
                (self.list_id, theme),
            )
        except sqlite3.IntegrityError as e:
            LOGGER.warning(
                "Inserting existing theme %r in list %s: %r",
                theme,
                self.list_id,
                e,
                exc_info=e,
            )
            return False
        LOGGER.debug("Added theme %r to list %s", theme, self.list_id)
        return True

    def remove_theme(self, theme):
        self.__cursor.execute(
            "delete from themes where list_id = ? and name = ?", (self.list_id, theme)
        )
        deleted = self.__cursor.rowcount > 0
        LOGGER.debug(
            "Removed theme %r from list %s (deleted = %s)", theme, self.list_id, deleted
        )
        return deleted

    def themes(self):
        self.__cursor.execute(
            "select name from themes where list_id = ?", (self.list_id,)
        )
        return [theme for (theme,) in self.__cursor.fetchall()]

    def admins(self):
        self.__cursor.execute(
            "select users.user_id, private_chat_id, name"
            " from users join memberships"
            " on users.user_id = memberships.user_id"
            " where list_id = ? and is_admin",
            (self.list_id,),
        )
        us = self.__cursor.fetchall()
        return [ASUser(self.__cursor, *u) for u in us]

    def is_admin(self, user):
        if self.__admins is None:
            self.__admins = {u.user_id: u for u in self.admins()}
        return user.user_id in self.__admins


class ASUser:
    @staticmethod
    def create(cursor, user_id, private_chat_id, name=None):
        cursor.execute(
            "insert or replace into users"
            " (user_id, private_chat_id, name)"
            " values (?, ?, ?)",
            (user_id, private_chat_id, name),
        )
        LOGGER.debug("Created new user: %s (name = {%r)", user_id, name)
        return ASUser(cursor, user_id, private_chat_id, name)

    @staticmethod
    def get(cursor, user_id=None, private_chat_id=None):
        assert (user_id is None) != (private_chat_id is None)
        if user_id is not None:
            cursor.execute(
                "select user_id, private_chat_id, name from users where user_id = ?",
                (user_id,),
            )
        else:
            cursor.execute(
                "select user_id, private_chat_id, name"
                " from users where private_chat_id = ?",
                (private_chat_id,),
            )
        u = cursor.fetchone()
        if not u:
            return None
        else:
            return ASUser(cursor, *u)

    def __init__(self, cursor, user_id, private_chat_id, name):
        self.__cursor = cursor
        self.__user_id = user_id
        self.__private_chat_id = private_chat_id
        self.name = name

    @property
    def user_id(self):
        return self.__user_id

    @property
    def private_chat_id(self):
        return self.__private_chat_id

    def delete(self):
        self.__cursor.execute("delete from users where user_id = ?", (self.user_id,))

    def update(self):
        self.__cursor.execute(
            "update users set name = ? where user_id = ?", (self.name, self.user_id)
        )


class AmigoSecreto:
    def __init__(self, dbname):
        LOGGER.debug("Opening %s", dbname)
        self.__conn = sqlite3.connect(dbname)
        self.__conn.executescript(CREATE_TABLES_SCRIPT)
        LOGGER.info("Opened %s", dbname)

    def commit(self):
        self.__conn.commit()

    def create_list(self, **kwargs):
        l = ASList.create(self.__conn.cursor(), **kwargs)
        self.__conn.commit()
        return l

    def get_list(self, **kwargs):
        l = ASList.get(self.__conn.cursor(), **kwargs)
        return l

    def create_user(self, **kwargs):
        u = ASUser.create(self.__conn.cursor(), **kwargs)
        self.__conn.commit()
        return u

    def get_user(self, **kwargs):
        u = ASUser.get(self.__conn.cursor(), **kwargs)
        return u

    def draw(self, **kwargs):
        the_list = self.get_list(**kwargs)
        users = the_list.users()
        themes = the_list.themes()

        LOGGER.info(
            "Drawing pairings between %d users, with %d themes: %r",
            len(users),
            len(themes),
            themes,
        )

        if len(themes) < len(users):
            LOGGER.warning("Less themes than users; some pairs will have no theme")
            themes += [None] * (len(users) - len(themes))

        targets = santa_shuffle(len(users))
        random.shuffle(themes)

        pairs = [
            {"from": users[source], "to": users[target], "theme": theme}
            for (source, (target, theme)) in enumerate(zip(targets, themes))
        ]
        assert len(pairs) == len(users)
        return pairs
